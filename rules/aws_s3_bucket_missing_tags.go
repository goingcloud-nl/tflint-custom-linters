package rules

import (
	"fmt"
	"sort"
	"strings"

	hcl "github.com/hashicorp/hcl/v2"
	"github.com/terraform-linters/tflint-plugin-sdk/hclext"
	"github.com/terraform-linters/tflint-plugin-sdk/logger"
	"github.com/terraform-linters/tflint-plugin-sdk/tflint"
	"github.com/terraform-linters/tflint-ruleset-aws/project"
	"github.com/terraform-linters/tflint-ruleset-aws/rules/tags"
	"github.com/zclconf/go-cty/cty"
)

// AwsS3BucketMissingTagsRule checks whether resources are tagged correctly
type AwsS3BucketMissingTagsRule struct {
	tflint.DefaultRule
}

type awsResourceTagsRuleConfig struct {
	Tags []string `hclext:"tags"`
}

const (
	tagsAttributeName = "tags"
	tagBlockName      = "tag"
)

// NewAwsS3BucketMissingTagsRule returns new rules for all resources that support tags
func NewAwsS3BucketMissingTagsRule() *AwsS3BucketMissingTagsRule {
	return &AwsS3BucketMissingTagsRule{}
}

// Name returns the rule name
func (r *AwsS3BucketMissingTagsRule) Name() string {
	return "aws_s3_bucket_missing_tags"
	// return "aws_resource_missing_tags"
}

// Enabled returns whether the rule is enabled by default
func (r *AwsS3BucketMissingTagsRule) Enabled() bool {
	return false
}

// Severity returns the rule severity
func (r *AwsS3BucketMissingTagsRule) Severity() tflint.Severity {
	return tflint.NOTICE
}

// Link returns the rule reference link
func (r *AwsS3BucketMissingTagsRule) Link() string {
	return project.ReferenceLink(r.Name())
}

// Check checks resources for missing tags
func (r *AwsS3BucketMissingTagsRule) Check(runner tflint.Runner) error {
	config := awsResourceTagsRuleConfig{}
	if err := runner.DecodeRuleConfig(r.Name(), &config); err != nil {
		return err
	}

	for _, resourceType := range tags.Resources {
		if resourceType == "aws_s3_bucket" {
			resources, err := runner.GetResourceContent(resourceType, &hclext.BodySchema{
				Attributes: []hclext.AttributeSchema{{Name: tagsAttributeName}},
			}, nil)
			if err != nil {
				return err
			}

			for _, resource := range resources.Blocks {
				if attribute, ok := resource.Body.Attributes[tagsAttributeName]; ok {
					logger.Debug("Walk `%s` attribute", resource.Labels[0]+"."+resource.Labels[1]+"."+tagsAttributeName)
					resourceTags := make(map[string]string)
					wantType := cty.Map(cty.String)
					err := runner.EvaluateExpr(attribute.Expr, &resourceTags, &tflint.EvaluateExprOption{WantType: &wantType})
					err = runner.EnsureNoError(err, func() error {
						r.emitIssue(runner, resourceTags, config, attribute.Expr.Range())
						return nil
					})
					if err != nil {
						return err
					}
				} else {
					logger.Debug("Walk `%s` resource", resource.Labels[0]+"."+resource.Labels[1])
					r.emitIssue(runner, map[string]string{}, config, resource.DefRange)
				}

			}
		}
	}
	return nil
}

func (r *AwsS3BucketMissingTagsRule) emitIssue(runner tflint.Runner, tags map[string]string, config awsResourceTagsRuleConfig, location hcl.Range) {
	var missing []string
	for _, tag := range config.Tags {
		if _, ok := tags[tag]; !ok {
			missing = append(missing, fmt.Sprintf("\"%s\"", tag))
		}
	}
	if len(missing) > 0 {
		sort.Strings(missing)
		wanted := strings.Join(missing, ", ")
		issue := fmt.Sprintf("The resource is missing the following tags: %s.", wanted)
		runner.EmitIssue(r, issue, location)
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
